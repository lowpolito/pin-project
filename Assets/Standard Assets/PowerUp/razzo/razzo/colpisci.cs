﻿using UnityEngine;
using System.Collections;

public class colpisci : MonoBehaviour {

	//private GameObject razzo;
	private NavMeshAgent nav;               
	private GameObject[] enemiesObject;
	private float distance;
	private GameObject enemyFound;
	public ParticleSystem explosion_smoke;
	private Vector3 offset;
	private bool spara = false;
	float t;
	int Mioid;
	public GameObject Macchine;
	public AudioSource lancio;

	public void Spara( int id, int dir)
	{ 
		t = Time.time;
		Mioid = id;
		nav = GetComponent <NavMeshAgent> ();
		enemyFound = Macchine.GetComponent<take_position> ().getEnemyToKill (id, dir);
		spara = true;
		Debug.Log (enemyFound.name);

		lancio.Play ();
	}

	private void OnTriggerEnter(Collider other) //metodo usato quando c'è collisione con oggetto magico
	{
		//se il razzo colpisce un'altra macchina questa salta
		if (other.gameObject==enemyFound)
		{
			other.GetComponent<Rigidbody>().AddForce(Vector3.up*1000,ForceMode.Acceleration);
			other.GetComponent<Rigidbody> ().transform.Rotate (new Vector3(45,45,45));
			lancio.Stop ();
			esplodi ();
		}

	}



	// Update is called once per frame
	void FixedUpdate () {
		if (spara) {
			nav.SetDestination (enemyFound.transform.position);
			if (Time.time - t > 3) {
//				gameObject.GetComponent<Rigidbody> ().useGravity = true;
//				gameObject.GetComponent<NavMeshAgent> ().enabled = false;
				esplodi();
				spara = false;
			}
		}
	}

	private void esplodi(){
		explosion_smoke.transform.position =transform.position;
		explosion_smoke.Play();
		explosion_smoke.GetComponent<AudioSource> ().Play ();
		Destroy (this.gameObject);
	}	
}