﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PowerUpManager: MonoBehaviour {
	//particelle rainbow del barile
	public ParticleSystem Smoke_particle_barile_red;
	public ParticleSystem Smoke_particle_barile_yellow;
	public ParticleSystem Smoke_particle_barile_blue;
	public ParticleSystem Smoke_particle_barile_green;
	public ParticleSystem Smoke_particle_barile_cyano; //colore preferito di Gian

	private ParticleSystem Smoke_particle_barile_red_clone;
	private ParticleSystem Smoke_particle_barile_yellow_clone;
	private ParticleSystem Smoke_particle_barile_blue_clone;
	private ParticleSystem Smoke_particle_barile_green_clone;
	private ParticleSystem Smoke_particle_barile_cyano_clone; //colore preferito di Gian


	private float time_begin_barile; // variabile dove è salvato il tempo quando viene toccato il barile
	public bool barileHit; // barile colpito sì o no
	public bool isPalla;
	private Rigidbody pallaClone;
	public Rigidbody terremotoR;
	private Rigidbody terremotoRClone;

	private Terremoto terremoto;
	private bool isTerremoto;
	float t;


	public Text PowerUpText; //Text in cui viene mostrato il power up selezionato (in futuro sarà un'immagine)
	//inizializzo Power UP
	public Rigidbody razzo; //Seleziona il refab del razzo per poterlo clonare all'occorrenza
	public Rigidbody palla;

	private enum PowerUp //Qui sono presenti tutti i tipi di power up
	{
		Razzo, Palla, BarileVegan, Terremoto, Nessuno
	};
	private PowerUp CurrentPowerUp;
	private List < GameObject > macchine;

	private GameObject figliogiusto;

	// Use this for initialization
	void Start() {
		figliogiusto = gameObject;
		CurrentPowerUp = PowerUp.Nessuno;
		if (figliogiusto.transform.GetChild(0).tag == "Player") {
			PowerUpText.text = "";
		}
		foreach(Transform child in gameObject.transform) {
			if (child.gameObject.tag == "Player" || child.gameObject.tag == "enemy") {
				figliogiusto = child.gameObject;
			}
		}
		macchine = new List < GameObject > ();
		foreach(Transform child in gameObject.transform.parent.transform) {
			macchine.Add(child.gameObject);
		}
		barileHit = false;
		terremoto = new Terremoto();
		isTerremoto = false;
	}

	// Update is called once per frame
	void Update() {
		if (isPalla) {
			Quaternion r = new Quaternion(15.0f, figliogiusto.transform.rotation.y, figliogiusto.transform.rotation.z, figliogiusto.transform.rotation.w);
			Vector3 t = new Vector3(figliogiusto.transform.position.x, figliogiusto.transform.position.y, figliogiusto.transform.position.z);
			GameObject.FindGameObjectWithTag("MainCamera").transform.rotation = r;
			GameObject.FindGameObjectWithTag("MainCamera").transform.position = t;
		}
	}

	private void FixedUpdate() {
		if (isPalla) {
			//			pallaClone.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y+3, gameObject.transform.position.z);
		}
		//barile PU for 3 second
		if (barileHit) {
			if (Time.time - time_begin_barile > 3) {
				barileHit = false;
				//stop all the particles system for barile PU 
				Smoke_particle_barile_red_clone.Stop();
				Smoke_particle_barile_yellow_clone.Stop();
				Smoke_particle_barile_green_clone.Stop();
				Smoke_particle_barile_blue_clone.Stop();
				Smoke_particle_barile_cyano_clone.Stop();
			}
		}
		if (isTerremoto) {
			Debug.Log("In Uso");
			if (Time.time - t > 4) {
				Vibration.Cancel();
				isTerremoto = false;
			} else {
				foreach(GameObject m in macchine) {
					if (m.GetInstanceID() != gameObject.GetInstanceID()) {
					foreach(Transform child in m.transform) {
							if (child.gameObject.tag == "Player" || child.gameObject.tag == "enemy") {
								child.gameObject.GetComponent < Rigidbody > ().AddForce (Vector3.up * UnityEngine.Random.Range (-2, 2) * 500, ForceMode.Impulse);
								child.gameObject.GetComponent < Rigidbody > ().AddForce (Vector3.back * UnityEngine.Random.Range (-2, 2) * 500, ForceMode.Impulse);
								if (UnityEngine.Random.Range (-2, 2) > 0) {
									child.gameObject.GetComponent < Rigidbody > ().AddForce (Vector3.right * UnityEngine.Random.Range (-4, 4) * 500, ForceMode.Impulse);
								} else {
									child.gameObject.GetComponent < Rigidbody > ().AddForce (Vector3.left * UnityEngine.Random.Range (-4, 4) * 500, ForceMode.Impulse);
								}
							}
						}
					}
				}
			}

		}
		if (figliogiusto.tag == "enemy") {
			if (UnityEngine.Random.Range (-2, 2) > 0) {
				if (CurrentPowerUp != PowerUp.Nessuno) {
					usaPowerUp (0);
				}
			}
		}

	}

	public bool getBarileHit() {
		return barileHit;
	}

	public void AssegnaPowerUp() { //seleziona a random un powerUp da enum e lo asssegna a currentpowerup
		if (CurrentPowerUp == PowerUp.Nessuno) {
			Array values = Enum.GetValues(typeof(PowerUp));
			System.Random random = new System.Random();
			CurrentPowerUp = (PowerUp) values.GetValue(random.Next(values.Length - 1));
		}
		CurrentPowerUp = PowerUp.Razzo;
		if (figliogiusto.tag == "Player") {
			PowerUpText.text = CurrentPowerUp.ToString ();
		}
	}

	public void usaPowerUp() { //metodo chiamato quando si deve utilizzare un power up tra i possibili enumerati in PowerUp
		usaPowerUp(-1);
	}

	public void usaPowerUp(int dir) { //metodo chiamato quando si deve utilizzare un power up tra i possibili enumerati in PowerUp
		switch (CurrentPowerUp) {
		case PowerUp.Razzo:
			SparaRazzo(dir);
			break;
		case PowerUp.Palla:
//			DiventaPalla();
			usaVeganBarile();
			break;
		case PowerUp.BarileVegan:
			usaVeganBarile();
			break;
		case PowerUp.Terremoto:
			usaTerremoto();
			break;
		case PowerUp.Nessuno:
			break;
		}
		//	print ("usa power up, il currentpowerup è "+CurrentPowerUp.ToString());
		CurrentPowerUp = PowerUp.Nessuno;
		if (figliogiusto.tag == "Player") {
			PowerUpText.text = "";
		}
	}

	private void SparaRazzo(int dir) { //1 swipe up 0 swipe down
		//clono il prefab del razzo
		Vector3 t = figliogiusto.transform.position;
		t.z += 12.0f;
		Rigidbody RazzoClone = (Rigidbody) Instantiate(razzo, figliogiusto.transform.position, figliogiusto.transform.rotation);
		//RazzoClone.transform.position = t;
		//chiamo la funzione spara del clone
		//	print ("id del giocatore in carcontroller = " + gameObject.GetInstanceID());
		if (figliogiusto.tag != "Player") {
			if (GameObject.FindGameObjectWithTag ("macchine").GetComponent<take_position> ().getIndex (gameObject.GetInstanceID ()) == 0) {
				dir = 0;
			} else {
				dir = 1;
			}
		}
			RazzoClone.GetComponent < colpisci > ().Spara (gameObject.GetInstanceID (), dir);

	}

	//dovrei provare a togliere questa cazzo di macchina e sostituirla con la palla di quello spilungone di jordan!
	private void DiventaPalla() {
		pallaClone = (Rigidbody) Instantiate(palla, figliogiusto.transform.position, figliogiusto.transform.rotation);
		pallaClone.transform.parent = gameObject.transform;
		GameObject.FindGameObjectWithTag("MainCamera").transform.parent = gameObject.transform;
		figliogiusto.SetActive(false);
		figliogiusto = pallaClone.gameObject;
		figliogiusto.SetActive(true);
		isPalla = true;
	}

	public void usaVeganBarile() {
		Vibration.Vibrate(3000);
		barileHit = true;
		time_begin_barile = Time.time;

		Smoke_particle_barile_red_clone = (ParticleSystem) Instantiate(Smoke_particle_barile_red, figliogiusto.transform.position, figliogiusto.transform.rotation);
		Smoke_particle_barile_yellow_clone = (ParticleSystem) Instantiate(Smoke_particle_barile_yellow, figliogiusto.transform.position, figliogiusto.transform.rotation);
		Smoke_particle_barile_blue_clone = (ParticleSystem) Instantiate(Smoke_particle_barile_blue, figliogiusto.transform.position, figliogiusto.transform.rotation);
		Smoke_particle_barile_green_clone = (ParticleSystem) Instantiate(Smoke_particle_barile_green, figliogiusto.transform.position, figliogiusto.transform.rotation);
		Smoke_particle_barile_cyano_clone = (ParticleSystem) Instantiate(Smoke_particle_barile_cyano, figliogiusto.transform.position, figliogiusto.transform.rotation);

		Smoke_particle_barile_red_clone.transform.parent = figliogiusto.transform;
		Smoke_particle_barile_yellow_clone.transform.parent = figliogiusto.transform;
		Smoke_particle_barile_blue_clone.transform.parent = figliogiusto.transform;
		Smoke_particle_barile_green_clone.transform.parent = figliogiusto.transform;
		Smoke_particle_barile_cyano_clone.transform.parent = figliogiusto.transform;

		Smoke_particle_barile_red_clone.transform.localPosition = new Vector3(0.0f, 0.57f, -7.08f);
		Smoke_particle_barile_yellow_clone.transform.localPosition = new Vector3(0.0f, 0.57f, -7.08f);
		Smoke_particle_barile_blue_clone.transform.localPosition = new Vector3(0.0f, 0.57f, -7.08f);
		Smoke_particle_barile_cyano_clone.transform.localPosition = new Vector3(0.0f, 0.57f, -7.08f);
		Smoke_particle_barile_green_clone.transform.localPosition = new Vector3(0.0f, 0.57f, -7.08f);

		//	play all the particles system for barile PU
		Smoke_particle_barile_red_clone.Play();
		Smoke_particle_barile_yellow_clone.Play();
		Smoke_particle_barile_blue_clone.Play();
		Smoke_particle_barile_green_clone.Play();
		Smoke_particle_barile_cyano_clone.Play();

	}

	public void usaTerremoto() {
		terremotoRClone = (Rigidbody) Instantiate(terremotoR, figliogiusto.transform.position, figliogiusto.transform.rotation);
		terremoto = terremotoRClone.GetComponent < Terremoto > ();
		terremoto.init();
		terremoto.usaTerremoto();
		isTerremoto = true;
		t = Time.time;
		Vibration.Vibrate(3000);
	}
}