﻿using UnityEngine;
using System.Collections;

public class BoxController : MonoBehaviour {
	float t;

	void Start(){
	}

	void Update () 
	{
		if (Time.time - t > 3) {
			gameObject.GetComponent<MeshRenderer> ().enabled = true;
			gameObject.GetComponent<BoxCollider> ().enabled = true;
		}

	}

	private void OnTriggerEnter(Collider other) //metodo usato quando c'è collisione con box
	{   
		t = Time.time;
		gameObject.GetComponent<MeshRenderer> ().enabled = false;
		gameObject.GetComponent<BoxCollider> ().enabled = false;

		}
}