using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
	[RequireComponent(typeof (CarController))]
	public class CarUserControl : MonoBehaviour
	{
		private CarController m_Car; // the car controller we want to use
		public Animator anim;
		public float coefficienteSterzoAccelerometro = 1f;
		public PowerUpManager parent;
		private Gamemenager GMS;


		private void Awake()
		{
			// get the car controller
			m_Car = GetComponent<CarController>();
			parent=gameObject.transform.parent.gameObject.GetComponent<PowerUpManager>();
			GMS = GameObject.Find ("Gamemenager").GetComponent<Gamemenager>();
		}


		private void FixedUpdate()
		{   if (GMS.countdownDown==true) {
				// pass the input to the car!
				float h = CrossPlatformInputManager.GetAxis ("Horizontal");
				float v = CrossPlatformInputManager.GetAxis ("Vertical");
				SterzaAnim (h);

				if (Input.GetKey ("f")) {
					parent.usaPowerUp ();
				}

				//			Debug.Log("Bool: " + anim.GetBool("isTurning") + " " + "hAnim: " + anim.GetFloat("h") + " h: " + h);
				#if MOBILE_INPUT
			h = (Input.acceleration.x)*coefficienteSterzoAccelerometro;
			SterzaAnim(h);
			float handbrake = CrossPlatformInputManager.GetAxis("Jump");
			m_Car.Move(h, v, v, handbrake);
				#else
				m_Car.Move (h, v, v, 0f);
				SterzaAnim (h);
				#endif

			}
		}

		void OnTap(Vector2 TapPos){
			print (TapPos.x);
		}


		void OnSwipeUp() //1 swipe up 0 swipe down
		{
			parent.usaPowerUp (1);
		}

		void OnSwipeDown() //1 swipe up 0 swipe down
		{
			parent.usaPowerUp (0);
		}

		private void SterzaAnim(float h) 
		{
			if (!parent.isPalla) {
				// vedo se sto girando, lo notifico all'animator e gli dico se sto girando a destra/sinistra per far partire l'animazione su Pinoe
				if (h != 0) {
					anim.SetBool ("isTurning", true);
					// 
					float h2 = (h / 2) + 0.5f; //porto la soglia da [-1,1] a [0,1]
					anim.SetFloat ("h", h2);
				} else if (h == 0) {
					anim.SetBool ("isTurning", false);
				}
			}
		}
	}
}
