﻿using UnityEngine;
using UnityStandardAssets.Vehicles.Car;
using UnityEngine.UI;
using System.Collections.Generic;
using System;


public class take_position : MonoBehaviour {

	public Text posizione; //Text area in cui viene visualizzata la posizione del giocatore
	public Text currentGiro;
	public List<GameObject> macchine;
	private int idPlayer;
	public GameObject MacchinaPlayer;

	// Use this for initialization
	void Start () {
		// ai = GameObject.FindGameObjectWithTag("macchine").GetComponent<car_AI_script>();
		macchine = getMacchine ();
	}
	
	// Update is called once per frame
	void Update () {
/*		foreach (GameObject m in macchine) {
			foreach(Transform child in m.transform){
				if(child.gameObject.tag == "Player" || child.gameObject.tag == "enemy"){
					m.transform.position = child.transform.position;
				}
			}
		}
*/		SortBeppe (macchine);
		posizione.text = getPos(MacchinaPlayer.GetInstanceID()) + "°";
//		currentGiro.text = "Lap: " + MacchinaPlayer.GetComponent<Car_Artificial_Intelligence>().getcurrentGiro().ToString();
		Debug.Log("NomeMacchina: " + MacchinaPlayer.name + " Target: " + MacchinaPlayer.GetComponent<Car_Artificial_Intelligence>().getTarget_i() + " Giro: " + MacchinaPlayer.GetComponent<Car_Artificial_Intelligence>().getcurrentGiro().ToString());
	}
		

	public static List<GameObject> getMacchine(){
		List<GameObject> macchine = new List<GameObject>();
		GameObject go = GameObject.FindGameObjectWithTag ("macchine");
		foreach (Transform tran in go.transform)
		{
			macchine.Add (tran.gameObject);
/*			foreach(Transform child in tran.transform){
				if(child.gameObject.tag == "Player" || child.gameObject.tag == "enemy"){
					macchine.Add(child.gameObject);
				}
			}
*/		}
		return macchine;

	}
		

	public bool isAvanti(GameObject c1, GameObject c2){
		Car_Artificial_Intelligence c1_ai = c1.GetComponent<Car_Artificial_Intelligence>();
		Car_Artificial_Intelligence c2_ai = c2.GetComponent<Car_Artificial_Intelligence>();
	
		if (c1_ai.getcurrentGiro() > c2_ai.getcurrentGiro()) {
			return true;
		}

		if(c1_ai.getTarget_i() > c2_ai.getTarget_i()) { //se l'indice del target a cui punta il Player è più grande di quello del nemico allora vuol dire che il Player è davanti
			return true; //quindi decremento la posizione
		} else if (c1_ai.getTarget_i() == c2_ai.getTarget_i()) {//se invece i target sono gli stessi 
			Vector3 target_comune = c1_ai.getTarget().transform.position;
			foreach(Transform child in c1.transform){
				if(child.gameObject.tag == "Player" || child.gameObject.tag == "enemy"){
					c1 = child.gameObject;
				}
			}
			foreach(Transform child in c2.transform){
				if(child.gameObject.tag == "Player" || child.gameObject.tag == "enemy"){
					c2 = child.gameObject;
				}
			}
			Vector3 PTD = c1.transform.InverseTransformPoint(target_comune); //prendo la distanza fra il Player e il suo target PlayerTargetDistance
			Vector3 ETD = c2.transform.InverseTransformPoint(target_comune); //prendo la distanza fra il Nemico e il suo target EnemyTargetDistance
			if (PTD.magnitude < ETD.magnitude) { // Se la distanza tra il target è il Player è più piccola di quella tra il target e il Nemico allora il player è davanti
				return true; //quindi decremento la posizione
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
		

	public void SortBeppe(List <GameObject> macchine){
		for (int i = 0; i < macchine.Count; i++) {
			for (int j = i+1; j < macchine.Count; j++) {
				if(!isAvanti(macchine[i], macchine[j])) {
					GameObject temp = macchine [i];
					macchine [i] = macchine [j];
					macchine [j] = temp;
				}
			}
		}
		/*for (int i = 0; i < macchine.Count; i++) {
			int j = i + 1;
			Debug.Log (j + "° " + macchine [i].name);
		}*/
	}

	public int getPos(int idPlayer) {
		for (int i = 0; i < macchine.Count; i++) {
			if (macchine [i].GetInstanceID () == idPlayer)
				return i+1;
		}
		return 0;
	}

	public int getIndex(int idPlayer) {
		print("richiesta l'index di "+idPlayer);
		for (int i = 0; i < macchine.Count; i++) {
			if (macchine [i].GetInstanceID () == idPlayer) {
				print ("ritorno l'indice :" + i);
				return i;
			}
		}
		return 0;
	}

	public GameObject getEnemyToKill(int idPlayer, int dir){
		
		print ("id del giocatore in enemytokill = " + idPlayer);
		int indexPlayer = getIndex (idPlayer);
		print ("qui ho ricevuto l'index ed è " + indexPlayer);
		if (dir == 0) {
			indexPlayer++;
		} else {
			indexPlayer--;
		}

		if (indexPlayer < 0) {
			indexPlayer++;
			indexPlayer++;
		} else if (indexPlayer > macchine.Count - 1) {
			indexPlayer--;
			indexPlayer--;
		}

		print ("indexdel nemico aggiornato a " + indexPlayer);
		GameObject c1 = macchine [indexPlayer];
		foreach(Transform child in c1.transform){
			if(child.gameObject.tag == "Player" || child.gameObject.tag == "enemy"){
				c1 = child.gameObject;
			}
		}
		return c1;
	}

}