Here you have the most fancy game we have ever made! (even because is our first one!)

In order to try it you can clone the repository, open the project with Unity and build the game for your OS, oooor more easily you can download the Windows and Mac OS builds by clicking this link
https://bitbucket.org/lowpolito/pin-project/downloads/pino%20builds.zip

Enjoy! :)