﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Markermanager : MonoBehaviour {

	public List<GameObject> targets;
	public Transform m_Target;
	private int target_i;
	public GameObject car;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public int getTarget_i(){
		return target_i;
	}
	public void SetTarget(Transform target)
	{
		m_Target = target;

	}

	public GameObject getTarget(){
		return targets[getTarget_i()];
	}

	public static List<GameObject> getMarkers(){
		GameObject go = GameObject.Find ("Markers");
		List<GameObject> markers = new List<GameObject>();
		foreach (Transform tran in go.transform)
		{
			markers.Add(tran.gameObject);
		}
		return markers;

	}
}
