﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class QuitApplication : MonoBehaviour {

	public void Quit()
	{
		//If we are running in a standalone build of the game
	#if UNITY_STANDALONE
		//Quit the application
		SceneManager.LoadScene(0);
	#endif

		//If we are running in the editor
	#if UNITY_EDITOR
		//Stop playing the scene
		SceneManager.LoadScene(0);
	
	#endif
	}
}
